﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
	

	public float timeCounter;
	public float scoreCounter;
	public Canvas canvas;
	Text textLabel;
	Text ballLabel;
	Text scoreLabel;
	public bool endGame;


	BallSpawner ballSpawner;

	void Start(){
		ballSpawner = GetComponent<BallSpawner> ();
		Text[] textLabels=canvas.GetComponentsInChildren <Text>();
		for (int i = 0; i < 2; i++) {
			
			if (textLabels[i].name=="Timer"){
				textLabel=textLabels[i];
			}
			if (textLabels[i].name=="numBalls"){
				ballLabel=textLabels[i];
			}

		}
		

	}
	void Update() {
		if (!endGame) {
			scoreCounter += Time.deltaTime;
			timeCounter += Time.deltaTime;

			var minutes = timeCounter / 60; //Divide the guiTime by sixty to get the minutes.
			var seconds = timeCounter % 60;//Use the euclidean division for the seconds.
			var fraction = (timeCounter * 100) % 100;

			//update the label value
			textLabel.text = string.Format ("{0:00} : {1:00} : {2:000}", minutes, seconds, fraction);

			ballLabel.text = ballSpawner.numBalls + "X";
		} else {
			/* This is what crashed it before.
			 * I have the logic but i dont know how to implement
			scoreLabel.text = "Score" + (numballs * Seconds);
			*/
		}
			
	}
}