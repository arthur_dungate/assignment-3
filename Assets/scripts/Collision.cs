﻿using UnityEngine;
using System.Collections;

public class Collision : MonoBehaviour {
	bool active;
	public bool endGame;
	void Start() {
		StartCoroutine(delay());

	}


	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.gameObject.tag == "Player") Debug.Log(active);
		if (active &&  collision.gameObject.tag == "Player"){
			collision.gameObject.GetComponent<Timer> ().endGame = true;
			collision.gameObject.GetComponent<Move> ().enabled = false;

		}
		if(collision.gameObject.tag != "Player") active=true;
	}



	IEnumerator delay() {
		
		yield return new WaitForSeconds(1);
		active = true;
	
	
	}

}