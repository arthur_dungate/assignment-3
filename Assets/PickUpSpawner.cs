﻿using UnityEngine;
using System.Collections;

public class PickUpSpawner : MonoBehaviour {

	public int numP = 0;
	public GameObject pickupPrefab;
	public float offSetRangemax = 1.6f;
	public float offSetRangemin = 1.5f;

	// Use this for initialization
	void Update() {
		if (numP < 1)
			SpawnPickup();


	}


	void SpawnPickup()
	{

		Vector2 spawnOffset = new Vector2 (Random.Range(-offSetRangemax, offSetRangemin), Random.Range(-offSetRangemax, offSetRangemin));
		Instantiate(pickupPrefab, (Vector2)transform.position + spawnOffset, Quaternion.identity);
		numP += 1;

	}

	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.gameObject.tag == "Player");{
			Destroy (pickupPrefab);
			numP -= 1;
			/* and take off ten seconds from the timer but i have no idea how */
		}
	}
}
